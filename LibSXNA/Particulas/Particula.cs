﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LibSXNA.Particulas
{
    class Particula
    {
        // Imagem da partícula
        public Texture2D Texture { get; set; }
        // Posição da partícula no plano X,Y
        public Vector2 Position { get; set; }
        // Velocidade linear da partícula
        public Vector2 Velocity;// { get; set; }
        // Aceleração no eixo X
        public float aceleracaoX { get; set; }
        // Velocidade angular da partícula
        public float AngularVelocity { get; set; }
        // Angulo atual de desenho partícula
        public float Angle { get; set; }
        // Cor da partícula
        public Color Color { get; set; }
        // Tamanho da partícula
        public float Size { get; set; }
        // Tempo de vida restante da partícula
        public float tempoVidaRestante { get; set; }
        // Tempo de vida da partícula
        public float tempoVida { get; set; }

        public Particula(Texture2D texture, Vector2 position, float aceleracaoX, Vector2 velocity,
            float angle, float angularVelocity, Color color, float size, float lifeTime)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            this.aceleracaoX = aceleracaoX;
            this.tempoVida = lifeTime;
            this.tempoVidaRestante = lifeTime;
        }

        public void Update(GameTime gameTime)
        {
            tempoVidaRestante--;
            Velocity.X = Velocity.X + aceleracaoX * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Position += Velocity;
            Angle += AngularVelocity;
        }

        public void Draw(Camera camera)
        {
            // normalized lifetime is a value from 0 to 1 and represents how far
            // a particle is through its life. 0 means it just started, .5 is half
            // way through, and 1.0 means it's just about to be finished.
            // this value will be used to calculate alpha and scale, to avoid 
            // having particles suddenly appear or disappear.
            //(1 - p) / p
            float tempoVidaNormalizado = (this.tempoVida - this.tempoVidaRestante) / this.tempoVida;

            // we want particles to fade in and fade out, so we'll calculate alpha
            // to be (normalizedLifetime) * (1-normalizedLifetime). this way, when
            // normalizedLifetime is 0 or 1, alpha is 0. the maximum value is at
            // normalizedLifetime = .5, and is
            // (normalizedLifetime) * (1-normalizedLifetime)
            // (.5)                 * (1-.5)
            // .25
            // since we want the maximum alpha to be 1, not .25, we'll scale the 
            // entire equation by 4.
            float alpha = 4 * tempoVidaNormalizado * (1 - tempoVidaNormalizado);
            Color cor = Color.White * alpha;

            // make particles grow as they age. they'll start at 75% of their size,
            // and increase to 100% once they're finished.
            float scale = Size * (.3f + .8f * tempoVidaNormalizado);

            // Define o retângulo de recorte da imagem,
            Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            // Define a origem da imagem (centro)
            Vector2 origin = new Vector2(Texture.Width / 2, Texture.Height / 2);
            // Desenha a imagem
            camera.RenderDraw(Texture,
                Position,
                sourceRectangle,
                cor,
                Angle,
                origin,
                scale,
                SpriteEffects.None);
        }

    }
}
