﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LibSXNA.Particulas
{
    public enum TipoEmissor
    {
        Fumaca,
        Vapor,
        ExplosaoMadeira
    }

    public class GerenciadorDeParticula
    {

        // Valor randomicoico
        private Random _randomico;
        // Posição X,Y do emissor de partículas
        private Vector2 _localEmissor;
        // Lista de partículas
        private List<Particula> _particulas;
        // Lista de imagens para as partículas
        private List<Texture2D> _texturas;
        // Total de partículas
        private int _totalParticulasPorSegundo;
        // Total de particulas que serão geradas
        private int _quantidadeParticulas;
        // Tempo de vida base das partículas
        private int _tempoDeVidaBase;
        //
        private int _emissaoPorSegundo;
        // Tipo do emissor (1- Fumaça, 2- Vapor)
        private TipoEmissor _tipoEmissor;

        private Vector2 _minVelocidade;

        private Vector2 _maxVelocidade;

        private float _minVelocidadeAngular;
        private float _maxVelocidadeAngular;

        private float _minAceleracao;
        private float _maxAceleracao;

        public Vector2 Posicao
        {
            get { return _localEmissor; }
            set { _localEmissor = value; }
        }

        public Vector2 VelocidadeMin
        {
            get { return _minVelocidade; }
            set { _minVelocidade = value; }
        }

        public Vector2 VelocidadeMax
        {
            get { return _maxVelocidade; }
            set { _maxVelocidade = value; }
        }

        public float VelocidadeAngularMin
        {
            get { return _minVelocidadeAngular; }
            set { _minVelocidadeAngular = value; }
        }

        public float VelocidadeAngularMax
        {
            get { return _maxVelocidadeAngular; }
            set { _maxVelocidadeAngular = value; }
        }

        public float AceleracaoMin
        {
            get { return _minAceleracao; }
            set { _minAceleracao = value; }
        }

        public float AceleracaoMax
        {
            get { return _maxAceleracao; }
            set { _maxAceleracao = value; }
        }

        public int QuantidadeParticulas
        {
            //Só retorna 0 quanto n tiver mas nenhuma particula na fila ou em execução
            get { return _particulas.Count + _quantidadeParticulas; }
        }

        // Construtor do Gerenciador de Partículas
        public GerenciadorDeParticula(TipoEmissor tipoEmissor, List<Texture2D> texturas, Vector2 posicao, Vector2 minVelocidade, Vector2 maxVelocidade, float minAceleracao, float maxAceleracao,
            float minVelocidadeAngular, float maxVelocidadeAngular, int quantidadeParticulas /*0 = infinito*/, int totalParticulasPorSegundo, int tempoDeVidaBase, int emissaoPorSegundo)
        {
            this._tipoEmissor = tipoEmissor;
            this._localEmissor = posicao;
            this._texturas = texturas;
            this._totalParticulasPorSegundo = totalParticulasPorSegundo;
            this._quantidadeParticulas = quantidadeParticulas;
            this._tempoDeVidaBase = tempoDeVidaBase;
            this._emissaoPorSegundo = 1 / emissaoPorSegundo;
            this._particulas = new List<Particula>();
            _randomico = new Random((int)(posicao.X + posicao.Y));

            _minAceleracao = minAceleracao;
            _maxAceleracao = maxAceleracao;

            _minVelocidade = minVelocidade;

            _maxVelocidade = maxVelocidade;

            _minVelocidadeAngular = minVelocidadeAngular;
            _maxVelocidadeAngular = maxVelocidadeAngular;

            random = new Random((int)(posicao.X + posicao.Y));
        }

        private float _tempoTotal = 0;

        // Atualização do gerenciador de partículas
        public void Update(GameTime gameTime)
        {
            if (_quantidadeParticulas > 0)
            {
                //Converte para float pois o valor é recebido em double
                _tempoTotal += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (_tempoTotal > _emissaoPorSegundo)
                {
                    for (int i = 0; i < _totalParticulasPorSegundo; i++)
                    {
                        switch (_tipoEmissor)
                        {
                            case TipoEmissor.Fumaca:
                                _particulas.Add(CriarParticulaFumaca());
                                break;

                            case TipoEmissor.Vapor:
                                break;

                            case TipoEmissor.ExplosaoMadeira:
                                _particulas.Add(CriarParticulaExplosaoMadeira());
                                break;

                            default:
                                //particulas.Add(CreateParticleRandomly());
                                break;
                        }

                        _quantidadeParticulas--;
                    }

                    _tempoTotal = 0;
                }

            }

            /* Verifica se acabou o tempo de vida da partícula,
             se acabou, elimina a partícula dando espaço para novas
             partículas serem adicionadas */
            for (int particula = 0; particula < _particulas.Count; particula++)
            {
                _particulas[particula].Update(gameTime);

                if (_particulas[particula].tempoVidaRestante <= 0)
                {
                    _particulas.RemoveAt(particula);
                    particula--;
                }
            }
        }

       
        // Cria uma nova partícula com movimento de fumaça
        private Particula CriarParticulaFumaca()
        {
            // Seleciona uma imagem aleatória da lista
            Texture2D textura = _texturas[_randomico.Next(_texturas.Count)];
            // Posição do emissor de partículas
            Vector2 posicao = _localEmissor;
            // Aceleração aleatória
            float aceleracaoX = RandomBetween(_minAceleracao, _maxAceleracao);
            // Velocidade aleatória da partícula
            Vector2 velocidade = new Vector2(RandomBetween(_minVelocidade.X, _maxVelocidade.X),
                                            RandomBetween(_minVelocidade.Y, _maxVelocidade.Y));

            // Angulo de rotação da partícula
            float angulo = 0;
            // Velocidade randomicoica angular da partícula
            float velocidadeAngular = RandomBetween(_minVelocidadeAngular, _maxVelocidadeAngular);//0.1f * (float)(randomico.NextDouble() * 2 - 1);
            // Cor branca para não alterar a cor original das imagens
            Color cor = new Color(255, 255, 255);
            // Tamanho randomicoico da partícula
            float tamanho = 1f;//(float)randomico.NextDouble();
            // Tempo de vida da partícula
            int tempoDeVida = this._tempoDeVidaBase + _randomico.Next(40);

            return new Particula(textura, posicao, aceleracaoX, velocidade, angulo, velocidadeAngular, cor, tamanho, tempoDeVida);
        }

        // Cria uma nova partícula com movimento de explosão da caixa de madeira
        private Particula CriarParticulaExplosaoMadeira()
        {
            /*// Seleciona uma imagem aleatória da lista
            Texture2D textura = _texturas[_randomico.Next(_texturas.Count)];
            // Posição do emissor de partículas
            Vector2 posicao = _localEmissor;
            // Aceleração aleatória
            float aceleracaoX = RandomBetween(_minAceleracao, _maxAceleracao);
            // Velocidade aleatória da partícula
            Vector2 velocidade = new Vector2(RandomBetween(_minVelocidade.X, _maxVelocidade.X),
                                            RandomBetween(_minVelocidade.Y, _maxVelocidade.Y));

            // Angulo de rotação da partícula
            float angulo = 0;
            // Velocidade randomicoica angular da partícula
            float velocidadeAngular = RandomBetween(_minVelocidadeAngular, _maxVelocidadeAngular);//0.1f * (float)(randomico.NextDouble() * 2 - 1);
            // Cor branca para não alterar a cor original das imagens
            Color cor = new Color(255, 255, 255);
            // Tamanho randomicoico da partícula
            float tamanho = 1f;//(float)randomico.NextDouble();
            // Tempo de vida da partícula
            int tempoDeVida = this._tempoDeVidaBase + _randomico.Next(40);

            return new Particula(textura, posicao, aceleracaoX, velocidade, angulo, velocidadeAngular, cor, tamanho, tempoDeVida);
             */
            // Seleciona uma imagem aleatória da lista
            Texture2D texture = _texturas[_randomico.Next(_texturas.Count)];
            // Posição do emissor de partículas
            Vector2 position = _localEmissor;//new Vector2(RandomBetween(_localEmissor.X, _localEmissor.X + 60),
                                            //RandomBetween(_localEmissor.Y, _localEmissor.Y + 60));
            // Velocidade aleatória da partícula
            Vector2 velocity = new Vector2(
                                    1f * (float)(_randomico.NextDouble() * 2 - 1),
                                    1f * (float)(_randomico.NextDouble() * 2 - 1));
            // Angulo de rotação da partícula
            float angle = 0;
            // Velocidade randomicoica angular da partícula
            float angularVelocity = 0.1f * (float)(_randomico.NextDouble() * 2 - 1);
            // Cor randomicoica da partícula
            Color color = Color.White; /*new Color(
                        (float)_randomico.NextDouble(),
                        (float)_randomico.NextDouble(),
                        (float)_randomico.NextDouble());*/
            // Tamanho randomicoico da partícula
            float size = (float)_randomico.NextDouble();
            // Tempo de vida da partícula
            int lifeTime = 20 + _randomico.Next(40);

            return new Particula(texture, position, 0, velocity, angle, angularVelocity, color, size, lifeTime);

        }


        /* Cria uma nova partícula randomicoica, se você quiser criar outro
         tipos de partículas, você deve mudar este método ou criar outros
         métodos próprios para o movimento de sua partícula 
        private Particula CreateParticleRandomly()
        {
            // Seleciona uma imagem aleatória da lista
            Texture2D texture = texturas[randomico.Next(texturas.Count)];
            // Posição do emissor de partículas
            Vector2 position = LocalEmissor;
            // Velocidade aleatória da partícula
            Vector2 velocity = new Vector2(
                                    1f * (float)(randomico.NextDouble() * 2 - 1),
                                    1f * (float)(randomico.NextDouble() * 2 - 1));
            // Angulo de rotação da partícula
            float angle = 0;
            // Velocidade randomicoica angular da partícula
            float angularVelocity = 0.1f * (float)(randomico.NextDouble() * 2 - 1);
            // Cor randomicoica da partícula
            Color color = new Color(
                        (float)randomico.NextDouble(),
                        (float)randomico.NextDouble(),
                        (float)randomico.NextDouble());
            // Tamanho randomicoico da partícula
            float size = (float)randomico.NextDouble();
            // Tempo de vida da partícula
            int lifeTime = 20 + randomico.Next(40);

            return new Particula(texture, position, velocity, angle, angularVelocity, color, size, lifeTime);
        }*/

        private Random random;

        public float RandomBetween(float min, float max)
        {
            
            return min + (float)random.NextDouble() * (max - min);
        }

        // Desenha todas as partículas gerenciadas
        public void Draw(Camera camera)
        {
            foreach (Particula p in _particulas)
                p.Draw(camera);
        }
    }
}
