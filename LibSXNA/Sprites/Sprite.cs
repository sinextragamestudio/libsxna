﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using LibSXNA.Sprites.Animacao;

namespace LibSXNA.Sprites
{
    public class Sprite
    {
        #region Variáveis

        private Texture2D _textura;

        private GerenciadorDeAnimacao _animacoes;

        private Vector2 _posicao = Vector2.Zero;

        private Vector2 _origem = Vector2.Zero;

        private Vector2 _velocidade = Vector2.Zero;

        private float _angulo = 0;

        private Color _cor = Color.White;

        private float _escala = 0;

        private float _peso = 0;

        // Define o retângulo de recorte da imagem,
        private Rectangle _sourceRectangle;

        private Vector2 _fatorDivisao = Vector2.Zero;

        private Camera _camera;

        #endregion

        #region Get/Set

        public Texture2D Textura
        {
            get { return _textura; }
            set { _textura = value;  }
        }

        public Vector2 Posicao
        {
            get { return _posicao; }
            set { _posicao = value; }
        }

        public Vector2 Velocidade
        {
            get { return _velocidade; }
            set { _velocidade = value; }
        }

        public Vector2 Origem
        {
            get { return _origem; }
            set { _origem = value; }
        }

        public float Peso
        {
            get { return _peso; }
            set { _peso = value * 3; }
        }

        public float Angulo
        {
            get { return _angulo; }
            set { _angulo = value; }
        }

        public float Escala
        {
            get { return _escala; }
            set { _escala = value; }
        }

        public Color Cor
        {
            get { return _cor; }
            set { _cor = value; }
        }

        public GerenciadorDeAnimacao Animacoes
        {
            get { return _animacoes; }
            set { _animacoes = value; }
        }

        public BoundingBox CorpoBB
        {
            get 
            { 
                if (_animacoes == null)
                    return new BoundingBox(new Vector3(_posicao.X, _posicao.Y, 0), new Vector3(_posicao.X + _textura.Width, _posicao.Y + _textura.Height, 0)); 
                else
                    return new BoundingBox(new Vector3(_posicao.X, _posicao.Y, 0), new Vector3(_posicao.X + _animacoes.animacoes[_animacoes.animacaoAtual].DimensaoFrame.X, _posicao.Y + _animacoes.animacoes[_animacoes.animacaoAtual].DimensaoFrame.Y, 0));
            }
        }

        public Rectangle Corpo
        {
            get
            {
                if (_animacoes == null)
                    return new Rectangle((int)_posicao.X, (int)_posicao.Y, _textura.Width, _textura.Height);
                else
                    return new Rectangle((int)_posicao.X, (int)_posicao.Y, (int)_animacoes.animacoes[_animacoes.animacaoAtual].DimensaoFrame.X, (int)_animacoes.animacoes[_animacoes.animacaoAtual].DimensaoFrame.Y);
            }
        }

        public Camera camera
        {
            get { return _camera; }
        }

        #endregion

        #region Métodos de Inicialização

        public Sprite()
        {
            this._posicao = Vector2.Zero;
            this._velocidade = Vector2.Zero;
        }

        public Sprite(Texture2D textura, Camera camera)
        {
            this._textura = textura;
            this._posicao = Vector2.Zero;
            // Define a origem da imagem (centro)
            _origem = new Vector2(textura.Width / 2, textura.Height / 2);
            this._sourceRectangle = new Rectangle(0, 0, textura.Width, textura.Height);

            this._camera = camera;

            this._velocidade = Vector2.Zero;

        }
        public Sprite(Texture2D textura, Vector2 posicao, Camera camera)
        {
            this._textura = textura;
            this._posicao = posicao;
            // Define a origem da imagem (centro)
            _origem = new Vector2(textura.Width / 2, textura.Height / 2);
            this._sourceRectangle = new Rectangle(0, 0, textura.Width, textura.Height);

            this._camera = camera;

            this._velocidade = Vector2.Zero;

            this._fatorDivisao = camera.FatorEscalaPosicao;
        }

        public Sprite(GerenciadorDeAnimacao animacoes, Vector2 posicao, float peso, Camera camera)
        {
            this._posicao = posicao;
            this._animacoes = animacoes;

            this._peso = peso;

            this._camera = camera;

            this._velocidade = Vector2.Zero;
        }

        public Sprite(Texture2D textura, Vector2 posicao, float angulo, float escala, Color cor, Camera camera)
        {
            this._textura = textura;
            this._posicao = posicao;
            this._angulo = angulo;
            this._escala = escala;
            this._cor = cor;

            // Define a origem da imagem (centro)
            _origem = new Vector2(textura.Width / 2, textura.Height / 2);
            this._sourceRectangle = new Rectangle(0, 0, textura.Width, textura.Height);

           // this._corpo = new BoundingBox(new Vector3(_posicao.X, _posicao.Y, 0), new Vector3(_posicao.X + textura.Width, _posicao.Y + textura.Height, 0));

            this._fatorDivisao = camera.FatorEscalaPosicao;
            this._camera = camera;
        }

        #endregion
    }
}
