﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LibSXNA.Sprites.Animacao
{
    public class GerenciadorDeAnimacao
    {
        //Dicionário de animações
        public Dictionary<string, Animacao> animacoes = new Dictionary<string, Animacao>();
        //Variavel com a animação rodando atualmente
        public string animacaoAtual;

        Camera _camera;

        //public Vector2 posicao = Vector2.Zero;

        public GerenciadorDeAnimacao(Camera camera) {
            this._camera = camera;
        }

        public void AddAnimacao(Animacao animacao, string nome)
        {
            animacoes.Add(nome, animacao);
        }

        public void trocaAnimacao(string nome)
        {
            animacaoAtual = nome;
        }

        public void Update(GameTime gameTime)
        {
            animacoes[animacaoAtual].Update(gameTime);
        }

        public void Draw(Vector2 posicao, SpriteEffects orientacao)
        {
            animacoes[animacaoAtual].Draw(posicao, orientacao, _camera);
        }
    }
}
