﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LibSXNA.Sprites.Animacao
{
    public class Animacao
    {
        private Texture2D _textura;

        private int _framesPorSegundo;

        private float _tempoPorFrame;

        private float _tempoTotal;
        //Variavel semelhante a um Vector2, porém com menos métodos
        private Vector2 _tamanhoFrame;

        private int _frameAtual;

        private int _tamanhoImagem;

        private Point _posicaoInicial;

        private bool _isLoop;

        private bool _isFim = false;

        //private Camera _camera;

        public bool IsLoop
        {
            set { _isLoop = value; }
        }

        public Vector2 DimensaoFrame
        {
            get { return _tamanhoFrame; }
        }

        public int FrameAtual
        {
            set { _frameAtual = value; }
            get { return _frameAtual; }
        }

        public bool Finalizada
        {
            get { return _isFim; }
        }

        public bool Reset
        {
            set { _isFim = !value;
            _frameAtual = 0;
            }
        }

        public Animacao(Texture2D textura, int framesPorSegundo, int colunas,
            int larguraFrame, int alturaFrame, Point posicaoInicial)//, Camera camera)
        {

            this._textura = textura;
            this._framesPorSegundo = framesPorSegundo;

            this._tempoPorFrame = 1 / (float)framesPorSegundo;

            this._tamanhoImagem = colunas;
            //this._tamanhoFrame = new Vector2((larguraFrame / camera.FatorTextura.X), (alturaFrame / camera.FatorTextura.Y));
            this._tamanhoFrame = new Vector2(textura.Width / colunas, textura.Height);

            this._frameAtual = 0;

            this._isLoop = false;

            this._posicaoInicial = posicaoInicial;

            //this._camera = camera;

        }

        public void Update(GameTime gameTime)
        {
            //Converte para float pois o valor é recebido em double
            _tempoTotal += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (_tempoTotal > _tempoPorFrame && _isFim == false)
            {
                _frameAtual++;
                if (_frameAtual >= _tamanhoImagem)
                {
                    if (_isLoop)
                    {
                        _frameAtual = 0;
                    }
                    else
                    {
                        _frameAtual--;
                        _isFim = true;
                    }
                }

                _tempoTotal = 0;
            }

        }

        public void Draw(Vector2 posicao, SpriteEffects orientacao, Camera _camera)
        {
            _camera.RenderDraw(_textura, posicao, new Rectangle((int) (_posicaoInicial.X + (_frameAtual * _tamanhoFrame.X)),(int) (_posicaoInicial.Y), (int) (_tamanhoFrame.X), (int) (_tamanhoFrame.Y)), 0, Vector2.Zero, orientacao);

            /*renderizador.Draw(_textura, posicao, new Rectangle(_posicaoInicial.X + (_frameAtual * _tamanhoFrame.X),
                _posicaoInicial.Y, _tamanhoFrame.X, _tamanhoFrame.Y),
                Color.White, 0, Vector2.Zero, 1, orientacao, 0);*/
        }

        public void Draw(Vector2 posicao, float rotacao, Vector2 origem, SpriteEffects orientacao, Camera _camera)
        {
            _camera.RenderDraw(_textura, posicao, new Rectangle((int) (_posicaoInicial.X + (_frameAtual * _tamanhoFrame.X)), (int) (_posicaoInicial.Y), (int) (_tamanhoFrame.X), (int) (_tamanhoFrame.Y)), rotacao, origem, orientacao);

            /*renderizador.Draw(_textura, 
                              posicao, 
                              new Rectangle(_posicaoInicial.X + (_frameAtual * _tamanhoFrame.X), _posicaoInicial.Y, _tamanhoFrame.X, _tamanhoFrame.Y),
                              Color.White, 
                              rotacao, 
                              origem, 
                              1, 
                              orientacao, 
                              0);*/
        }

        public void Draw(Vector2 posicao, float rotacao, Vector2 origem, Color cor, SpriteEffects orientacao, Camera _camera)
        {
            _camera.RenderDraw(_textura, posicao, new Rectangle((int) (_posicaoInicial.X + (_frameAtual * _tamanhoFrame.X)), (int) (_posicaoInicial.Y), (int) (_tamanhoFrame.X), (int) (_tamanhoFrame.Y)), cor, rotacao, origem, orientacao);

        }
    }
}
