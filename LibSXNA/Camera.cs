﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LibSXNA
{
    public enum ModoCamera
    {
        Quadrante, //Posiciona a camera de acordo com o quadrante do mapa (proporcional a resolução) que o jogador estiver
        Centralizado //Centraliza a câmera na posição repassada
    }

    public class Camera
    {

        #region Variáveis

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _render;
        private Vector2 _resolucaoBaseTextura;// = new Vector2(1280, 720);
        private Vector2 _resolucaoBasePosicao;// = new Vector2(1280, 720);
        private Vector2 _resolucao;
        private Vector2 _posicao;
		private Vector2 _offset = Vector2.Zero;
        private Vector2 _origem;
        private Rectangle? _limitesCamera;
        private Vector2 _dimensoesLevel;
        private float _zoom;
        private float _minZoom = 0.1f;
        private float _maxZoom = 1f;
        private float _rotacao;
        private Vector2 _escala = Vector2.One;
        private Vector2 _escalaPosicao = Vector2.One;
        private Vector2 _escalaDimensao = Vector2.One;
        //private Matrix _viewMatrix;
        private ModoCamera _modo;

        #endregion

        #region SET/GET

        public GraphicsDeviceManager Graphics
        {
            get { return _graphics; }
        }

        public Vector2 ResolucaoBaseTextura
        {
            get { return _resolucaoBaseTextura; }
        }

        public Vector2 ResolucaoJanela
        {
            get { return _resolucao; }
        }

        public Vector2 DimensaoCena
        {
            get { return _dimensoesLevel; }
        }

        public SpriteBatch Render
        {
            get { return _render; }
        }

        public Vector2 Posicao
        {
            get { return _posicao; }
            set 
            {
                _posicao = value;
                ValidarPosicao();
            }
        }

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = MathHelper.Max(value, _minZoom);
                ValidarZoom();
                ValidarPosicao();
            }
        }

        public float Rotacao
        {
            get { return _rotacao; }
            set { _rotacao = value; }
        }

        public Rectangle? LimitesCamera
        {
            get { return _limitesCamera; }
            set
            {
                this._limitesCamera = value;
                ValidarZoom();
                ValidarPosicao();
            }
        }

        public Vector2 FatorEscalaPosicao
        {
            get { return _escalaPosicao; }
        }

        public Vector2 FatorEscalaTextura
        {
            get { return _escala; }
            //set { _escala = value; }
        }

        public Vector2 FatorEscalaTexturaReversa
        {
            get { return Vector2.One / _escala; }
            //set { _escala = value; }
        }

        //Retorna quanto a mais a textura teria q ter para estar no tamanho original
        public Vector2 FatorTextura
        {
            get { return _escalaDimensao; }
            //set { _escala = value; }
        }

        //Retorna quanto a menos a textura tem do tamanho original
        public Vector2 FatorTexturaReverso
        {
            get { return Vector2.One / _escalaDimensao; }
            //set { _escala = value; }
        }

		public Vector2 Offset {
			get { return _offset; }
		}

        public Matrix ViewMatrix
        {
            get
            {
                return
                              Matrix.CreateTranslation(new Vector3(-_posicao.X, -_posicao.Y, 0f)) *
                              //Matrix.CreateTranslation(new Vector3(-_origem, 0f)) *
                    // Matrix.CreateRotationZ(_rotacao) *
                              Matrix.CreateScale(_zoom, _zoom, 1f) *
                              Matrix.CreateTranslation(new Vector3(0, 0, 0f)); //Matrix.CreateTranslation(new Vector3(_origem, 0f));
            }
        }

        #endregion

        #region Inicialização

        public Camera(Vector2 resolucaoBaseTextura, Vector2 resolucaoBasePosicao, GraphicsDeviceManager graphics, SpriteBatch render, Vector2 dimensoesLevel, Rectangle? limites, Vector2 resolucao, ModoCamera modo, Vector2 offset)
        {
            //graphics.PreferredBackBufferWidth = (int)resolucao.X;
            //graphics.PreferredBackBufferHeight = (int)resolucao.Y;
            //ativar v-sync quando full hd ou full screen para evitar que tenha problema de visualização da renderização do frame
            //graphics.SynchronizeWithVerticalRetrace = false;
            //graphics.ApplyChanges();
            _graphics = graphics;

			_offset = offset;

            _resolucaoBaseTextura = resolucaoBaseTextura;
            _resolucaoBasePosicao = resolucaoBasePosicao;

            _resolucao = resolucao;
            _dimensoesLevel = dimensoesLevel;

            _escala = _resolucao / _resolucaoBaseTextura;
            _escalaPosicao = (_resolucao / _resolucaoBasePosicao);

            _escalaDimensao = _resolucaoBasePosicao / _resolucaoBaseTextura;

            //Quando implementar com uma resolução base diferente, a ser considerada na escala, criar um novo fator, com resolução base em fullhd para ser considerado pela posição

            //v_Zoom = 1;
            _rotacao = 0;
            _origem = new Vector2(resolucao.X / 2.0f, resolucao.Y / 2.0f); //new Vector2(_graphics.GraphicsDevice.Viewport.Width / 2f, _graphics.GraphicsDevice.Viewport.Height / 2f);

            if (limites.HasValue)
            {
                limites = new Rectangle(
                    (int)(limites.Value.X * FatorEscalaPosicao.X * fatorEscalaFundo), 
                    (int)(limites.Value.Y * FatorEscalaPosicao.Y * fatorEscalaFundo), 
                    (int)(limites.Value.Width * FatorEscalaPosicao.X * fatorEscalaFundo), 
                    (int)(limites.Value.Height * FatorEscalaPosicao.Y * fatorEscalaFundo));
            }

            _limitesCamera = limites;

            _render = render;

            Zoom = 1;// FatorEscalaDimensao;
            
            Posicao = Vector2.Zero;

            _modo = modo;

            velocidadeDeslizeZoom = zoom * 3;
        }

        public void Reset()
        {
            Zoom = 1;
            _rotacao = 0;
            Posicao = Vector2.Zero;
            _escala = _resolucao / _resolucaoBaseTextura;
            valorDeslizeZoom = 0;
            nivelZoom = 5;

            _flagMovimentoAutomaticoPosZoom = false;
            _flagMovimentoAutomaticoMultiplicador = false;
        }

        #endregion

        #region Validação

        private void ValidarPosicao()
        {
            if (_limitesCamera.HasValue)
            {
                Vector2 cameraWorldMin = Vector2.Transform(Vector2.Zero, Matrix.Invert(ViewMatrix));
                Vector2 cameraSize = new Vector2(_graphics.GraphicsDevice.Viewport.Width, _graphics.GraphicsDevice.Viewport.Height) / _zoom;
                Vector2 limitWorldMin = new Vector2(_limitesCamera.Value.Left, _limitesCamera.Value.Top);
                Vector2 limitWorldMax = new Vector2(_limitesCamera.Value.Right, _limitesCamera.Value.Bottom);
                Vector2 positionOffset = _posicao - cameraWorldMin;
                _posicao = Vector2.Clamp(cameraWorldMin, limitWorldMin, limitWorldMax - cameraSize) + positionOffset;
            }
            
        }

        private void ValidarZoom()
        {
            if (_limitesCamera.HasValue)
            {
                float minZoomX = (float)_graphics.GraphicsDevice.Viewport.Width / _limitesCamera.Value.Width;
                float minZoomY = (float)_graphics.GraphicsDevice.Viewport.Height / _limitesCamera.Value.Height;
                _zoom = MathHelper.Max(_zoom, MathHelper.Max(minZoomX, minZoomY));
                //Impede q o zoom ultrapasse o valor maximo
                //_zoom = MathHelper.Min(_zoom, _maxZoom);
            }

            //Console.WriteLine(_zoom);
        }

        #endregion

        private bool _flagMovimentoAutomaticoPosZoom = false;
        private bool _flagMovimentoAutomaticoMultiplicador = false;

        private float _movimentoAutomaticoVelocidade = 3;

        private bool _flagMovimentoPararY = false;

        public bool _flagAnularMoverPara = false;
        public bool _flagAnularControlarMovimento_Automatico = false;
        public bool _flagAnularMovimentoAutomaticoPosZoom = false;

        public void AnularMovimentos()
        {
            _flagAnularControlarMovimento_Automatico = true;
            _flagAnularMoverPara = true;
            _flagAnularMovimentoAutomaticoPosZoom = true;
        }

        public void Update(GameTime gameTime, Vector2 ponteiro, bool ponteiro_pressionado, bool canhao_ativo)
        {

            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

            ControlarZoom(Mouse.GetState(), delta);
            ControlarMovimento(ponteiro, ponteiro_pressionado, canhao_ativo, delta);


            //Movimentação automática realizada após o zoom
            if (_limitesCamera.HasValue)
            {
                if (_flagMovimentoAutomaticoPosZoom && !_flagAnularMovimentoAutomaticoPosZoom)
                {
                    Vector2 posicaoCentro = Posicao + ResolucaoJanela / 2;

                    if (posicaoCentro.X <= (_limitesCamera.Value.X + _limitesCamera.Value.Width) / 2)
                    {
                        posicaoCentro.X -= _movimentoAutomaticoVelocidade;
                    }
                    else if (posicaoCentro.X > (_limitesCamera.Value.X + _limitesCamera.Value.Width) / 2)
                    {
                        posicaoCentro.X += _movimentoAutomaticoVelocidade;
                    }

                    //Console.WriteLine(posicaoCentro.Y + " - limite: " + (_limitesCamera.Value.Y + _limitesCamera.Value.Height) / 2);

                    if (!_flagMovimentoPararY)
                    {
                        if (posicaoCentro.Y < 0 + ResolucaoJanela.Y / 2)//(_limitesCamera.Value.Y + _limitesCamera.Value.Height) / 2)
                        {
                            posicaoCentro.Y += _movimentoAutomaticoVelocidade / 2;

                            if (posicaoCentro.Y > 0 + ResolucaoJanela.Y / 2)
                            {
                                posicaoCentro.Y = ResolucaoJanela.Y / 2;
                                _flagMovimentoPararY = true;
                            }
                        }
                        else if (posicaoCentro.Y < 0 + ResolucaoJanela.Y / 2)//(_limitesCamera.Value.Y + _limitesCamera.Value.Height) / 2)
                        {
                            posicaoCentro.Y -= _movimentoAutomaticoVelocidade / 2;

                            if (posicaoCentro.Y > 0 + ResolucaoJanela.Y / 2)
                            {
                                posicaoCentro.Y = ResolucaoJanela.Y / 2;
                                _flagMovimentoPararY = true;
                            }
                        }
                    }

                    //OlharPara(posicaoCentro);

                    if (Posicao + ResolucaoJanela / 2 != posicaoCentro)
                    {
                        _flagMovimentoAutomaticoPosZoom = false;
                        _flagMovimentoPararY = false;
                    }
                }

            }
        }

        public void OlharPara(Vector2 ponto, Vector2 dimensaoLevel, float delta)
        {
            Vector2 _posicao = this._posicao;

            switch (_modo)
            {
                case ModoCamera.Centralizado:
                    _posicao.X = (ponto.X * FatorEscalaPosicao.X) - ResolucaoJanela.X / 2f;
                    _posicao.Y = (ponto.Y * FatorEscalaPosicao.Y) - ResolucaoJanela.Y / 2f;
                    break;

                case ModoCamera.Quadrante:
                    _posicao.X = (float)Math.Floor(ponto.X / _resolucaoBaseTextura.X) * _resolucaoBaseTextura.X; //verificar
                    _posicao.Y = (float)Math.Floor(ponto.Y / _resolucaoBaseTextura.Y) * _resolucaoBaseTextura.Y;
                    break;
            }

            if (_posicao.X <= 0)
            {
                _posicao.X = 0;
                //this.v_CameraPosicao.X = 0;
            }

            if (_posicao.X + ResolucaoJanela.X >= dimensaoLevel.X * FatorEscalaTextura.X)
            {
                _posicao.X = (dimensaoLevel.X * FatorEscalaTextura.X) - ResolucaoJanela.X;
                //this.v_CameraPosicao.X = v_DimensoesLevel.X - v_resolucao.X;
            }

            if (_posicao.Y <= 0)
            {
                _posicao.Y = 0;
                //this.v_CameraPosicao.Y = 0;
            }

            if (_posicao.Y + ResolucaoJanela.Y >= dimensaoLevel.Y * FatorEscalaTextura.Y)
            {
                _posicao.Y = (dimensaoLevel.Y * FatorEscalaTextura.Y) - ResolucaoJanela.Y;
                // this.v_CameraPosicao.Y = v_DimensoesLevel.Y - v_resolucao.Y;
            }

            Vector2 PosicaoDiferenca = _posicao - Posicao;

            Posicao += (PosicaoDiferenca * (delta * 3));
        }

        public void MoverPara(Vector2 ponto, Vector2 dimensaoLevel, float delta)
        {
            if (!_flagAnularMoverPara)
            {
                Vector2 _posicao = this._posicao;

                _posicao.X = (ponto.X * FatorEscalaPosicao.X) - ResolucaoJanela.X / 2f;
                _posicao.Y = (ponto.Y * FatorEscalaPosicao.Y) - ResolucaoJanela.Y / 2f;

                Vector2 PosicaoDiferenca = _posicao - Posicao;

                Posicao += (PosicaoDiferenca * (delta * 3));

                if (Posicao.X <= 0)
                {
                    this._posicao.X = 0;
                    //this.v_CameraPosicao.X = 0;
                }

                if (Posicao.X + ResolucaoJanela.X >= dimensaoLevel.X * FatorEscalaTextura.X)
                {
                    this._posicao.X = (dimensaoLevel.X * FatorEscalaTextura.X) - ResolucaoJanela.X;
                    //this.v_CameraPosicao.X = v_DimensoesLevel.X - v_resolucao.X;
                }

                if (Posicao.Y <= 0)
                {
                    this._posicao.Y = 0;
                    //this.v_CameraPosicao.Y = 0;
                }

                if (Posicao.Y + ResolucaoJanela.Y >= dimensaoLevel.Y * FatorEscalaTextura.Y)
                {
                    this._posicao.Y = (dimensaoLevel.Y * FatorEscalaTextura.Y) - ResolucaoJanela.Y;
                    // this.v_CameraPosicao.Y = v_DimensoesLevel.Y - v_resolucao.Y;
                }
            }
        }

        private float VelocidadeDeslocamentoCamera = 350;

        bool ponteiro_inicial_inicializado = false;
        Vector2 ponteiro_inicial = Vector2.Zero; //primeiro lugar onde foi clicado ou tocado na tela
        Vector2 VelocidadeDeslocamentoPonteiro = Vector2.Zero;
        Vector2 Aceleracao = Vector2.Zero;
        float multiplicadorVelocidadePonteiro = 1.2f;

        public void ControlarMovimento(Vector2 ponteiro, bool ponteiro_pressionado, bool canhao_ativo, float delta)
        {
            if (!canhao_ativo)
            {
                //Reseta tudo caso o ponteiro esteja despressionado
                if (!ponteiro_pressionado)
                {
                    ponteiro_inicial_inicializado = false;

                    if (!_flagAnularControlarMovimento_Automatico)
                    {

                        VelocidadeDeslocamentoPonteiro -= Aceleracao * delta;

                        if ((int)VelocidadeDeslocamentoPonteiro.X == 0 && (int)VelocidadeDeslocamentoPonteiro.Y == 0)
                        {
                            AnularMovimentos();
                        }

                        Vector2 posicao = Posicao + new Vector2((int)(VelocidadeDeslocamentoPonteiro.X), (int)(VelocidadeDeslocamentoPonteiro.Y));

                        /*if (posicao.X < 0 || posicao.X > LimitesCamera.Value.X + LimitesCamera.Value.Width)
                        {
                            AnularMovimentos();
                        }

                        if (!_flagMovimentoPararY)
                        {
                            if (posicao.Y <= 0)
                            {
                                posicao.Y += Math.Abs(VelocidadeDeslocamentoPonteiro.Y / 2);

                                if (posicao.Y >= 0)
                                    _flagMovimentoPararY = true;
                            }
                            else if (posicao.Y >= 0)
                            {
                                posicao.Y -= Math.Abs(VelocidadeDeslocamentoPonteiro.Y / 2);

                                if (posicao.Y <= 0)
                                    _flagMovimentoPararY = true;
                            }
                        }*/

                        Posicao = posicao;

                        if (Posicao.Y < -ResolucaoJanela.Y / 4)
                        {
                            Posicao = new Vector2(Posicao.X, -ResolucaoJanela.Y / 4);
                        }
                        if (Posicao.Y > ResolucaoJanela.Y / 4)
                        {
                            Posicao = new Vector2(Posicao.X, ResolucaoJanela.Y / 4);
                        }

                    }
                }

                //marca o primeiro local onde foi clicado / tocado
                if (!ponteiro_inicial_inicializado && ponteiro_pressionado)
                {
                    ponteiro_inicial = ponteiro;
                    ponteiro_inicial_inicializado = true;
                }


                if (ponteiro_inicial_inicializado && ponteiro_pressionado)
                {
                    AnularMovimentos();
                    _flagAnularControlarMovimento_Automatico = false;
                    _flagMovimentoPararY = false;

                    if (ponteiro.X < ponteiro_inicial.X)
                    {
                        VelocidadeDeslocamentoPonteiro.X = (ponteiro_inicial.X - ponteiro.X) * delta * multiplicadorVelocidadePonteiro;
                    }
                    else if (ponteiro.X > ponteiro_inicial.X)
                    {
                        VelocidadeDeslocamentoPonteiro.X = (ponteiro_inicial.X - ponteiro.X) * delta * multiplicadorVelocidadePonteiro;
                    }

                    if (ponteiro.Y < ponteiro_inicial.Y)
                    {
                        VelocidadeDeslocamentoPonteiro.Y = (ponteiro_inicial.Y - ponteiro.Y) * delta * multiplicadorVelocidadePonteiro;
                    }
                    else if (ponteiro.Y > ponteiro_inicial.Y)
                    {
                        VelocidadeDeslocamentoPonteiro.Y = (ponteiro_inicial.Y - ponteiro.Y) * delta * multiplicadorVelocidadePonteiro;
                    }

                    Aceleracao = VelocidadeDeslocamentoPonteiro;

                    Posicao = Posicao + VelocidadeDeslocamentoPonteiro;

                    if (Posicao.Y < -ResolucaoJanela.Y / 4)
                    {
                        Posicao = new Vector2(Posicao.X, -ResolucaoJanela.Y / 4);
                    }
                    if (Posicao.Y > ResolucaoJanela.Y / 4)
                    {
                        Posicao = new Vector2(Posicao.X, ResolucaoJanela.Y / 4);
                    }
                }


                //Movimentação com teclado

                KeyboardState teclado = Keyboard.GetState();

                Vector2 PosicaoReferenciaCamera = Posicao;

                if (teclado.IsKeyDown(Keys.Up) && (PosicaoReferenciaCamera.Y > -ResolucaoJanela.Y / 4))
                {
                    PosicaoReferenciaCamera.Y -= VelocidadeDeslocamentoCamera * delta;
                    AnularMovimentos();
                }
                else if (teclado.IsKeyDown(Keys.Down) && PosicaoReferenciaCamera.Y < ResolucaoJanela.Y / 4)
                {
                    PosicaoReferenciaCamera.Y += VelocidadeDeslocamentoCamera * delta;
                    AnularMovimentos();
                }
                else if (teclado.IsKeyDown(Keys.Left))
                {
                    PosicaoReferenciaCamera.X -= VelocidadeDeslocamentoCamera * delta;
                    AnularMovimentos();
                }
                else if (teclado.IsKeyDown(Keys.Right))
                {
                    PosicaoReferenciaCamera.X += VelocidadeDeslocamentoCamera * delta;
                    AnularMovimentos();
                }

                Posicao = PosicaoReferenciaCamera;

                //Console.WriteLine(camera.Posicao);
            }

        }

        private bool reduzirZoom = false;
        private bool aumentarZoom = false;

        private Vector2 PosicaoFocoZoom = Vector2.Zero;

        int nivelZoom = 5;

        float valorDeslizeZoom = 0;

        float ScrollValor = 0;

        float zoom = 0.15f;

        float velocidadeDeslizeZoom;// = 10;

        public void ControlarZoom(MouseState mouse, float delta)
        {

            float newZoom = Zoom;

            //Verifica se o scroll rodou para cima ou para baixo
            if (!reduzirZoom && !aumentarZoom)
            {
                if (mouse.ScrollWheelValue < ScrollValor)
                {
                    if (nivelZoom > 1)
                    {
                        reduzirZoom = true;
                        aumentarZoom = false;
                        PosicaoFocoZoom = Posicao;//camera.TelaParaMundo(new Vector2(Mouse.GetState().X, Mouse.GetState().Y));
                    }
                }
                else if (mouse.ScrollWheelValue > ScrollValor)
                {
                    if (nivelZoom <= 4)
                    {
                        reduzirZoom = false;
                        aumentarZoom = true;
                        PosicaoFocoZoom = Posicao;// camera.TelaParaMundo(new Vector2(Mouse.GetState().X, Mouse.GetState().Y));
                    }
                }
            }

            //Caso true, faz as operações para deslizar o zoom
            if (reduzirZoom || aumentarZoom)
            {
                if (valorDeslizeZoom < zoom)
                {
                    if (reduzirZoom)
                    {
                        newZoom -= velocidadeDeslizeZoom * delta;
                    }
                    else if (aumentarZoom)
                    {
                        newZoom += velocidadeDeslizeZoom * delta;
                    }

                    valorDeslizeZoom += velocidadeDeslizeZoom * delta;

                    Posicao = PosicaoFocoZoom;
                    //OlharPara(PosicaoFocoZoom);

                }
                else if (valorDeslizeZoom >= zoom)
                {
                    valorDeslizeZoom = 0;

                    if (reduzirZoom)
                        nivelZoom--;
                    else if (aumentarZoom)
                        nivelZoom++;

                    /*
                    if (FatorEscalaObjetos >= 1)
                    {
                        FatorEscalaObjetos = 1;
                    }
                    else if (FatorEscalaObjetos <= 0)
                    {
                        FatorEscalaObjetos = 0;
                    }
                    */

                    aumentarZoom = false;
                    reduzirZoom = false;

                    AnularMovimentos();

                    _flagAnularMovimentoAutomaticoPosZoom = false;


                    _flagMovimentoAutomaticoPosZoom = true;
                }
            }

            Zoom = newZoom;

            ScrollValor = mouse.ScrollWheelValue;

        }

        public Vector2 MundoParaTela(Vector2 mundoPosicao)
        {
            return Vector2.Transform(mundoPosicao, ViewMatrix);
        }

        public Vector2 TelaParaMundo(Vector2 telaPosicao)
        {
            return (Vector2.Transform(telaPosicao, Matrix.Invert(ViewMatrix)) / FatorEscalaPosicao);
        }

        //Transformara a posição passada para ficar de acordo com a origem da camera
        //Recebe como parametro a posição original do objeto
        public Vector2 Transformar(Vector2 Transformarar)
        {
            return new Vector2(Transformarar.X - _posicao.X,
                Transformarar.Y - _posicao.Y) * _escala;
        }

        #region Rotinas de Renderização

        public void RenderBegin(Effect shader)
        {

            //_graphics.GraphicsDevice.Clear(Color.Black);

            _render.Begin(
                SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                null,
                DepthStencilState.Default,
                RasterizerState.CullNone,
                shader,
                ViewMatrix
                );
        }

        public void RenderString(SpriteFont fonte, String texto, Vector2 posicao, Color cor)
        {
            _render.DrawString(fonte, texto, (posicao * FatorEscalaPosicao) + _offset, cor, 0, Vector2.Zero, _escala, SpriteEffects.None, 0);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, float angulo, Vector2 origem)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        Color.White,
                        angulo,
                        origem,
                        _escala,
                        SpriteEffects.None,
                        0.0f);   
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, float angulo, float escala, Vector2 origem)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        Color.White,
                        angulo,
                        origem,
                        _escala * escala,
                        SpriteEffects.None,
                        0.0f);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, float angulo, Vector2 escala, Vector2 origem)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        Color.White,
                        angulo,
                        origem,
                        _escala * escala,
                        SpriteEffects.None,
                        0.0f);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, Color cor, float angulo, float escala, Vector2 origem)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        cor,
                        angulo,
                        origem,
                        _escala * escala,
                        SpriteEffects.None,
                        0.0f);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, Color cor, float angulo, Vector2 origem)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        cor,
                        angulo,
                        origem,
                        _escala,
                        SpriteEffects.None,
                        0.0f);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, float angulo, Vector2 origem, SpriteEffects efeito)
        {
            _render.Draw(textura,
                        (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                        null,
                        Color.White,
                        angulo,
                        origem,
                        _escala,
                        efeito,
                        0.0f);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, Rectangle frame, float angulo, Vector2 origem, SpriteEffects efeito)
        {
            _render.Draw(textura,
                              (posicao * FatorEscalaPosicao) + _offset,//Transformar(posicao),
                              frame,
                              Color.White,
                              angulo,
                              origem,
                              _escala,
                              efeito,
                              0);
        }

        public void RenderDrawSemZoom(Texture2D textura, Vector2 posicao, Rectangle? frame, float angulo, Vector2 origem, float escala, SpriteEffects efeito)
        {
            _render.Draw(textura,
                              (posicao * FatorEscalaPosicao) + _offset,
                              frame,
                              Color.White,
                              angulo,
                              origem,
                              new Vector2(_escala.X + (1 - Zoom), _escala.Y + (1 - Zoom)),//1 + (1- Zoom.X),
                              efeito,
                              0);

            //Console.WriteLine(posicao * FatorEscalaDimensao);
            //Console.WriteLine("camera: " + Posicao);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, Rectangle? frame, Color cor, float angulo, Vector2 origem, SpriteEffects efeito)
        {
            _render.Draw(textura,
                              (posicao * FatorEscalaPosicao) + _offset,
                              frame,
                              cor,
                              angulo,
                              origem,
                              _escala,//1 + (1- Zoom.X),
                              efeito,
                              0);

            //Console.WriteLine(posicao * FatorEscalaDimensao);
            //Console.WriteLine("camera: " + Posicao);
        }

        public void RenderDraw(Texture2D textura, Vector2 posicao, Rectangle? frame, Color cor, float angulo, Vector2 origem, float escala, SpriteEffects efeito)
        {
            _render.Draw(textura,
                              (posicao * FatorEscalaPosicao) + _offset,
                              frame,
                              cor,
                              angulo,
                              origem,
                              _escala * escala,//1 + (1- Zoom.X),
                              efeito,
                              0);

            //Console.WriteLine(posicao * FatorEscalaDimensao);
            //Console.WriteLine("camera: " + Posicao);
        }

        public void RenderDrawFundo(Texture2D textura, Vector2 posicao, Rectangle? frame, float angulo, Vector2 origem, float escala, SpriteEffects efeito)
        {
            _render.Draw(textura,
                              (posicao * FatorEscalaPosicao) + _offset,// * fatorEscalaFundo,
                              frame,
                              Color.White,
                              angulo,
                              origem,
                              _escala * escala,// * fatorEscalaFundo,//1 + (1- Zoom.X),
                              efeito,
                              0);

            //Console.WriteLine(posicao * FatorEscalaDimensao);
            //Console.WriteLine("camera: " + Posicao);
        }
        public float fatorEscalaFundo = 1.6f;

        public void RenderEnd()
        {
            _render.End();
        }

        #endregion

    }
}
