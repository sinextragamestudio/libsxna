﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LibSXNA.Cenas
{
    public class GerenciadorDeCenas : DrawableGameComponent
    {
        Dictionary<CenaType, BaseCena> cenas;

        CenaType cenaAtual;

        Game game;

        BaseCena cena;

        public CenaType TipoCenaAtual
        {
            get { return cenaAtual; }
        }

        public BaseCena CenaAtual
        {
            get { return cenas[cenaAtual]; }
        }

        public GerenciadorDeCenas(Game game, CenaType tipo,  BaseCena cena)
            : base(game)
        {
            this.game = game;
            this.cena = cena;
            this.cenaAtual = tipo;
            //this.Components.Add(new FPSCounter(this));
        }

        public override void Initialize()
        {
            // TODO: Add your initialization code here
            cenas = new Dictionary<CenaType, BaseCena>();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            TrocarCena(cenaAtual, cena);

            base.LoadContent();
        }

        public void TrocarCena(CenaType proximaCena, BaseCena objetoProximaCena)
        {
            //Caso a cena atual seja diferente de vazia, remove do dicionário
            if (this.cenaAtual != CenaType.None)
                this.cenas.Remove(cenaAtual);
            // A cena atual recebe a próxima cena desejada
            cenaAtual = proximaCena;


            this.cenas.Add(proximaCena, objetoProximaCena);

        }

        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            this.cenas[cenaAtual].Update(gameTime); // Atualizando a cena atual

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //this.render.Begin();

            this.cenas[cenaAtual].Draw(gameTime); // Desenhando a cena atual
            
            //this.render.End();

            base.Draw(gameTime);
        }
    }
}
