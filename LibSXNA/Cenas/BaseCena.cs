﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using LibSXNA;

namespace LibSXNA.Cenas
{
    public enum FadeType
    {
        Fade_In,
        Fade_Out
    }

    public enum CenaType
    {
        None,
        Splash,
        Menu,
        Seletor,
        Game
    }

    public abstract class BaseCena
    {
        public ContentManager Content;

        //public SpriteBatch render;

        public Camera camera;

        Texture2D blankScreen;
        public float alpha = 1f;

        public bool fimFadeIn = false;
        public bool fimFateOut = false;

        public Game game;

        public bool Fim
        {
            get { return (fimFadeIn || fimFateOut); }
        }

        public BaseCena(Game game)
        {
            this.game = game;

            MediaPlayer.Stop();

            this.Content = game.Content;

            this.camera = (Camera)game.Services.GetService(typeof(Camera));

            this.camera.Reset();

            //this.render = (SpriteBatch)game.Services.GetService(typeof(SpriteBatch));

            blankScreen = Content.Load<Texture2D>("Texturas/Util/branco");
        }

        public void FadeUpdate(GameTime gameTime, FadeType fade)
        {
            //Diminui o alpha da tela
            //Some o preto, aparece a imagem
            if (fade == FadeType.Fade_In && !fimFadeIn)
            {
                alpha -= (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.0007f;
                if (alpha < 0.1)
                {
                    fimFadeIn = true;
                    alpha = 0;
                }
            }
            //Aumenta o alpha da tela
            //Aparece o preto, some a imagem
            else if (fade == FadeType.Fade_Out && !fimFateOut)
            {
                alpha += (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.0007f;
                if (alpha > 1)
                {
                    fimFateOut = true;
                }
            }

        }

        public void FadeReset()
        {
            //alpha = 0f;
            fimFadeIn = false;
            fimFateOut = false;
        }

        public void FadeDraw()
        {
            this.camera.Render.Draw(blankScreen, Vector2.Zero, new Color(0, 0, 0, alpha));
        }

        public void FadeDraw(SpriteBatch render)
        {
            render.Draw(blankScreen, Vector2.Zero, new Color(0, 0, 0, alpha));
        }

        public abstract void Update(GameTime gameTime);

        public abstract void Draw(GameTime gameTime);

    }
}
