﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LibSXNA.Level
{
    public class Mapa
    {

        //Imagem com os tiles
        public Texture2D tileset;
        //Vetor com as posições de cada tile do tileset
        public Rectangle[] tileFrames;
        //Matriz de tiles
        public List<Tile> tiles;//Tile[,] tiles;
        //Número de linhas da matriz de tiles
        public int linhas;
        //Número de colunas da matriz de tiles
        public int colunas;
        //Altura do tile
        public int tileAltura;
        //Largura do tile
        public int tileLargura;
        //Cria uma constante inteira para representar o tipo de um tile não andavel
        public const int TIPO_NAO_ANDAVEL = 0;

        //public World mundo_fisico;

        int[,] _matrizBase;

        //Método construtor para inicializar as variaveis
        public Mapa(Texture2D tileset, int[,] matrizBase/*, World mundo_fisico, int tileDimensao*/)
        {
            int tileDimensao = 60;

            //this.mundo_fisico = mundo_fisico;
            this._matrizBase = matrizBase;
            this.linhas = matrizBase.GetLength(0);
            this.colunas = matrizBase.GetLength(1);
            this.tileLargura = tileDimensao;
            this.tileAltura = tileDimensao;
            this.tiles = new List<Tile>();//new Tile[linhas, colunas];

            this.tileset = tileset;

            //gera os frames dos tiles
            this.GerarTileFrames();

            this.CarregarMapa();
        }

        public void GerarTileFrames()
        {
            //Armazena a quantidade de tiles da tileset
            int tileSprites = (this.tileset.Width / this.tileLargura) * (this.tileset.Height / this.tileAltura);

            this.tileFrames = new Rectangle[tileSprites];
            //Indice para contar as posições do array tileFrames
            int indice = 0;
            //Largura da imagem do tile
            int width = this.tileset.Width;
            //Altura da imagem do tile
            int height = this.tileset.Height;

            //Percorre todos os tiles da tileset
            for (int y = 0; y < height; y += this.tileAltura)
            {
                for (int x = 0; x < width; x += this.tileLargura)
                {
                    //Armazena as posições (rectangle) correspondentes a cada tipo de tile
                    this.tileFrames[indice] = new Rectangle(x, y, this.tileLargura, this.tileAltura);
                    
                    indice++;
                }
            }
        }

        public void CarregarMapa(/*int[,] matriz, int[,] matrizColisao*/)
        {
            /* testa se a matriz parâmetro tem
            * dimensões diferentes da matriz de tiles
            */
            /*if (_matrizBase.GetLength(0) != this.tiles.GetLength(0) ||
            _matrizBase.GetLength(1) != this.tiles.GetLength(1))
            {
                throw new Exception("Matriz não possui a mesma dimensão do Mapa");
            }*/
            //testa se a imagem dos tiles foi carregada
            if (this.tileset == null)
            {
                throw new Exception("Imagem do mapa não foi carregada");
            }

            /* Testa se a matriz colisão tem
              dimensões diferentes da matriz de tiles */
            /*if (matrizColisao.GetLength(0) != this.tiles.GetLength(0) ||
            matrizColisao.GetLength(1) != this.tiles.GetLength(1))
            {
                throw new Exception("Matriz de Colisão não possui a mesma dimensão do Mapa");
            }*/

            //posição x do tile
            int x = 0;
            //posição y do tile
            int y = 0;
            //Tipo do tile que vai ser lido da matriz (inicializa com -1)
            int tipo = -1;
            //indica se o tile pode ser atravessado
            bool andavel = true;

            for (int i = 0; i < this.linhas; i++)
            {
                for (int j = 0; j < this.colunas; j++)
                {
                    //Atribui a "tipo" o tipo (número) do tile
                    tipo = _matrizBase[i, j];
                    //Console.WriteLine(tipo + ", linha = "+i+", coluna = "+j);
                    //Verifica se a posição da matriz é menor que zero ou maior que a quantidade de tiles ("frames")
                    if (tipo <= 0 || tipo > this.tileFrames.Length)
                    {
                        //throw new Exception("Tipo de tile inválido");
                    }
                    else
                    {
                        //Testa se o tipo lido é não-andável
                        //Em nosso exemplo temos apenas um, porém poderia-se criar um vetor com os tiles que não são andáveis e realizar a verificação
                        //if (matrizColisao[i, j] == TIPO_NAO_ANDAVEL)
                            andavel = false;

                        //Cria o tile na posição x, y de nossa janela com as suas dimensões
                        //this.tiles[i, j] = new Tile(tipo, andavel, x, y, this.tileLargura, this.tileAltura);
                        this.tiles.Add(new Tile(tipo, andavel, x, y, this.tileLargura, this.tileAltura));
                    }
                    //"Reseta" a variável para true
                    andavel = true;
                    //Soma em X a largura do tile
                    x += this.tileLargura;
                }
                //"Reseta" a posição no eixo X
                x = 0;
                //Soma em Y a altura do tile
                y += this.tileAltura;
            }
        }

        public void Draw(SpriteBatch batch, Camera camera)
        {
            foreach (Tile t in tiles)
            {
                int frame = t.Tipo;

                //Vector2 posicao = camera.Transformar(new Vector2(t.posicao.X, t.posicao.Y));
                //t.posicao.X = (int) posicao.X;
                //t.posicao.Y = (int) posicao.Y;

                //Console.WriteLine("ui - frame: "+this.tileFrames[frame - 1]+" - Posicao: "+posicao);

                //-1 colocado devido o mappy gerar com 1 a mais
                //batch.Draw(this.tileset, posicao, this.tileFrames[frame - 1], Color.White);

                camera.RenderDraw(this.tileset, t.Posicao, this.tileFrames[frame -1], Color.White, 0, Vector2.Zero, 1, SpriteEffects.None);
            }
            /*for (int i = 0; i < this.linhas; i++)
            {
                for (int j = 0; j < this.colunas; j++)
                {
                    if (this.tiles[i, j] != null)
                    {
                        int frame = this.tiles[i, j].tipo;
                        //Trace.WriteLine("Valor de i e j = " + i.ToString() + ", " + j.ToString() + " frame = "+frame.ToString() +" posicao no tileset = " + tileFrames[frame].X+", "+tileFrames[frame].Y);
                        Console.WriteLine("i =" + i);
                        //-1 colocado devido o mappy gerar com 1 a mais
                        batch.Draw(this.tileset, this.tiles[i, j].retangulo, this.tileFrames[frame - 1], Color.White);
                    }
                }
            }*/
        }

    }
}
