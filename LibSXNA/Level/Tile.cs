﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace LibSXNA.Level
{

    public class Tile
    {
        private Vector2 _posicao;
        //Posição para verificar o ponto de colisão (dentro da imagem)
        //private Rectangle posicaoPiso;
        //numeração usada na matriz
        private int _tipo;
        private bool _andavel;

        private Rectangle _corpo;

        private Rectangle _corpoPiso;

        public Vector2 Posicao
        {
            get { return _posicao; }
            set { _posicao = value; }
        }

        public bool Andavel
        {
            get { return _andavel; }
            set { _andavel = value; }
        }

        public Rectangle Corpo
        {
            get { return _corpo; }
        }

        public Rectangle CorpoPiso
        {
            get { return _corpo; }
        }

        public int Tipo
        {
            get { return _tipo;  }
        }

        public Tile(int tipo, bool andavel, int x, int y, int larguraTile, int alturaTile)
        {
            this._tipo = tipo;
            this._andavel = andavel;
            this._posicao = new Vector2(x, y);

            this._corpo = new Rectangle(x, y, larguraTile, alturaTile);

            //valor para decidir quantos pixels serão "desconsiderados" na altura do tile
            int diferenca = alturaTile / 5;

            this._corpoPiso = new Rectangle(x, y + diferenca, larguraTile, alturaTile - diferenca);
        }
    }
}
